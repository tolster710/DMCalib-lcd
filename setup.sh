#Install dependencies
sudo apt-get update
sudo apt-get install -y build-essential python-dev python-smbus python-pip git i2c-tools
sudo pip install RPi.GPIO

#Install adafruit libraries
mkdir ~/Documents/Git && cd ~/Documents/Git
git clone https://github.com/adafruit/Adafruit_Python_CharLCD.git
cd Adafruit_Python_CharLCD
sudo python setup.py install

#Clone code
#cd ~/Documents/Git
#git clone https://gitlab.com/tolster710/DMCalib-lcd.git lcd
#cd lcd
