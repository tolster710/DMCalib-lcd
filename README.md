# LCD Display

Simple library to write data to and clear the LCD char display


### Version
0.0.3

### Usage

Simple syntax for display:
default address 0x3f - commands can be chained together
```sh
$ python lcd_msg.py --address 0x3f
$ python lcd_msg.py --show [row] [col] [message text]
$ python lcd_msg.py --clear
```
### Installation
by running setup.sh we clone the git locally

```sh
$ git clone git@gitlab.com:tolster710/DMCalib-lcd.git lcd
$ cd lcd

```
### LCD  Wiring
*VCC --> 5v    (RPI 2)

*GND --> GND   (RPI 6)

*SDA --> I2c D (RPI 3)

*SCA --> Itc C (RPI 5)

#### LCD Used
(I2c LCD Display)[https://www.amazon.com/gp/product/B0182N00LU/ref=ox_sc_act_title_1?ie=UTF8&psc=1&smid=A36LYNH36T4]
