#!/usr/bin/python
import sys
sys.path.append("./lib")
import lcddriver
from time import *

import argparse
#import Adafruit_CharLCD as LCD
#from Adafruit_CharLCD import Adafruit_CharLCD
#import Adafruit_GPIO.MCP230xx as MCP

#from Adafruit_MCP230xx import MCP230XX_GPIO

# Raspberry Pi pin configuration:
'''
bus = 1
address = 0x20  # I2C address of the MCP230xx chip.
gpio_count = 8  # Number of GPIOs exposed by the MCP23008 chip

# Initialize MCP23017 device using its default 0x20 I2C address.
gpio = MCP.MCP23008(address, bus)

lcd_columns = 16
lcd_rows    = 2

lcd_rs        = 0
lcd_en        = 1
lcd_d4        = 2
lcd_d5        = 3
lcd_d6        = 4
lcd_d7        = 5

# Create MCP230xx GPIO adapter.
mcp = MCP230XX_GPIO(bus, address, gpio_count)
#lcd = Adafruit_CharLCD(pin_rs=1, pin_e=2, pins_db=[3,4,5,6], GPIO=mcp)
'''
#lcd = Adafruit_CharLCD(lcd_rs, lcd_en, lcd_d4, lcd_d5, lcd_d6, lcd_d7, lcd_columns, lcd_rows, gpio=mcp)
lcd = lcddriver.lcd()

#start main
parser = argparse.ArgumentParser()

parser.add_argument('-c', '--clear', default=False, action='store_true')
parser.add_argument('-s', '--show', nargs='+', help=' [Row_offset] [Col_offset] [message text]' )

args = parser.parse_args()

msg=False
if args.show:
        _list = args.show
        if len(_list) > 3:
                row = _list[0]
                col = _list[1]
                text = ' '.join(_list[2:])
        else:
                row, col, text = _list
        #print text, row, col
        row = int(row)
        col = int(col)
	lcd.setCursor(col,row)
	lcd.message(text)
elif args.clear:
        lcd.clear()

